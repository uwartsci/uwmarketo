CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Contributors


INTRODUCTION
------------

UW Marketo (uwmarketo) allows site administrators to embed a Marketo 
subscription form on their site. You can choose whether to show a direct 
subscription form or the preference center. There is a permission so you 
can delegate configuration of the form to any role. You must have an 
active Marketo subscription preference before you can successfully use 
this module.

For more information about Marketo at the University of 
Washington, contact mktohelp@uw.edu or see 
http://depts.washington.edu/uwadv/data-technology-resources/marketo/


REQUIREMENTS
------------

 * You must have established a Marketo subscription preference in the UW
 Preference Center before you can use this module. Contact mktohelp@uw.edu for
 assistance. To lookup your subscription or preference id, visit 
 https://subscribe.gifts.washington.edu/Admin/


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure module options in Administration » Configuration » User Interface » 
   UW Marketo. Configurable settings include the page title, the page path, 
   introductory text above the form, confirmation text above the form (shown after 
   user returns by clicking email link), and the subscription or preference center 
   id. There is also an option to disable the subscription form temporarily.

 * To allow others to administer this module, assign the "Administer UW Marketo 
   module" permission to a role in Administration » People » Permissions.


CONTRIBUTORS
------------

This module was originally created by the UW College of Arts & Sciences web team
(asweb@uw.edu). Contributions from the UW Drupal community are welcome. Please 
submit a pull request. The module repository is at 
https://bitbucket.org/uwartsci/uwmarketo
